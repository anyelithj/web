Página web de Pasos de Jesús

CREDITOS:
* Diseño Visual: Anyelith Jacobo, Paola Martinez, Vladimir Támara Patiño
* Estructura, contenido: Vladimir Támara Patiño vtamara@pasosdeJesus.org

Agradecemos a Dios por su provición continua.


Y llamó Abraham el nombre de aquel lugar, Jehová proveerá.
Por tanto se dice hoy: En el monte de Jehová será provisto.	
Genesis 22:14. 
